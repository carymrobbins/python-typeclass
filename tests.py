#!/usr/bin/env python
import datetime
from operator import add, mul
import unittest
from typeclass import (
    TypeClass, Maybe, Just, Nothing, State,
    fmap, ap, bind, then, lift_m, curry, auto_curry)


class TypeClassTest(unittest.TestCase):
    def test_nullable_dates_example(self):
        def add_nullables(x, y):
            mx, my = Maybe.from_nullables(x, y)
            return mx.ap(Just(add), my).to_nullable()
        # Both valid.
        date, delta = datetime.date.today(), datetime.timedelta(1)
        result = add_nullables(date, delta)
        self.assertEqual(result, date + delta)
        # First None
        date, delta = None, datetime.timedelta(2)
        result = add_nullables(date, delta)
        self.assertIsNone(result)
        # Second None.
        date, delta = datetime.date.today(), None
        result = add_nullables(date, delta)
        self.assertIsNone(result)
        # Both None.
        date, delta = None, None
        result = add_nullables(date, delta)
        self.assertIsNone(result)

    def test_curry_function(self):
        f = lambda x, y, z: x + y + z
        for g in (curry(3)(f), auto_curry(f)):
            self.assertEqual(g(1, 2, 3), f(1, 2, 3))
            self.assertEqual(g(1, 2)(3), f(1, 2, 3))
            self.assertEqual(g(1)(2)(3), f(1, 2, 3))

    def test_no_typeclass_instances(self):
        self.assertEqual(TypeClass.instances, {})

    def test_functor_instances(self):
        self.assertEqual(fmap(lambda x: x * 2, [1, 2, 3]), [2, 4, 6])
        self.assertEqual(fmap(lambda s: s + 'baz', dict(foo='bar', baz='quux')),
                         dict(foo='barbaz', baz='quuxbaz'))

    def test_applicative_instances(self):
        self.assertEqual(ap([lambda x: x + 1, lambda x: x + 2], [3, 4]),
                         [4, 5, 5, 6])
        self.assertEqual(ap([add, mul], [1, 2], [3, 4]),
                         [4, 5, 5, 6, 3, 4, 6, 8])

    def test_monad_instances(self):
        self.assertEqual(bind([1, 2, 3, 4], lambda x: [x, x + 1]),
                         [1, 2, 2, 3, 3, 4, 4, 5])
        self.assertEqual(then([1, 2], [3, 4]),
                         [3, 4, 3, 4])

    def test_maybe_functor(self):
        self.assertEqual(fmap(lambda x: x + 1, Just(2)), Just(3))
        self.assertEqual(fmap(lambda x: x + 1, Nothing), Nothing)

    def test_maybe_applicative(self):
        self.assertEqual(ap(Just(lambda x: x + 1), Just(2)), Just(3))
        self.assertEqual(ap(Just(lambda x: x + 1), Nothing), Nothing)
        self.assertEqual(ap(Nothing, Just(2)), Nothing)

    def test_maybe_monad(self):
        self.assertEqual(bind(Just(2), lambda x: Just(x + 1)), Just(3))
        self.assertEqual(bind(Just(2), lambda x: Nothing), Nothing)
        self.assertEqual(bind(Nothing, lambda x: Just(x + 1)), Nothing)
        self.assertEqual(then(Just(2), Just(5)), Just(5))
        self.assertEqual(then(Just(2), Nothing), Nothing)
        self.assertEqual(then(Nothing, Just(5)), Nothing)

    def test_maybe_to_and_from_nullable(self):
        self.assertEqual(Maybe.from_nullable(1), Just(1))
        self.assertEqual(Maybe.from_nullable(None), Nothing)
        self.assertEqual(Just(1).to_nullable(), 1)
        self.assertEqual(Nothing.to_nullable(), None)

    def test_maybe_monad_methods(self):
        self.assertEqual(Just(1).bind(lambda x: Just(x + 1)), Just(2))
        self.assertEqual(Just(1).bind(lambda _: Nothing), Nothing)
        self.assertEqual(Nothing.bind(lambda x: Just(x + 1)), Nothing)
        self.assertEqual(Just(1).then(Just(3)), Just(3))
        self.assertEqual(Just(1).then(Nothing), Nothing)
        self.assertEqual(Nothing.then(Just(1)), Nothing)
        self.assertEqual(Nothing.then(Nothing), Nothing)

    def test_state_monad(self):
        pop = State(lambda xs: (xs[0], xs[1:]))
        push = lambda x: State(lambda xs: ((), [x] + xs))
        s = push(3).then(pop).then(pop)
        result = s.run([5, 8, 2, 1])
        self.assertEqual(result, (5, [8, 2, 1]))
        s = pop >> pop >> push(3)
        _, result = s.run([5, 8, 2, 1])
        self.assertEqual(result, [3, 2, 1])

    def test_subclass_instance(self):
        class Foo(list):
            pass
        self.assertEqual(fmap(lambda x: x + 3, Foo([1, 2, 3])),
                         Foo([4, 5, 6]))

    def test_lift(self):
        f = lift_m(lambda x: x + 1)
        self.assertEqual(f(Just(2)), Just(3))
        self.assertEqual(f(Nothing), Nothing)


if __name__ == '__main__':

    unittest.main()
