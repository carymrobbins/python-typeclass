#!/usr/bin/env python
import abc
from functools import partial
import itertools
import types


def get_name(obj):
    return getattr(obj, '__name__', repr(obj))


class TypeClassMeta(abc.ABCMeta):
    def __new__(mcs, name, bases, attrs):
        maybe_type_param = filter(None, [getattr(base, 'type_param', None)
                                         for base in bases])
        if maybe_type_param:
            type_param = maybe_type_param[0]
            # Find superclass instances.
            abstract_methods = set(itertools.chain.from_iterable(
                getattr(base, '__abstractmethods__', []) for base in bases))
            if abstract_methods:
                for base in bases:
                    for sub in base.mro():
                        instance = getattr(sub, 'instances', {}).get(
                            type_param)
                        if instance:
                            for attr_name in abstract_methods:
                                attr = getattr(type(instance), attr_name,
                                               None)
                                if attr:
                                    if attr_name in attrs:
                                        pass
                                    attrs[attr_name] = attr
        result = super(TypeClassMeta, mcs).__new__(mcs, name, bases, attrs)
        if not 'type_param' in attrs:
            if hasattr(result, 'type_param'):
                # Create instance.
                result.instances[result.type_param] = (
                    super(TypeClassMeta, result).__call__())
            else:
                result.instances = {}
        return result

    def __call__(cls, type_param):
        name = '{}({})'.format(cls.__name__, get_name(type_param))
        return TypeClassMeta(name, (cls,), dict(type_param=type_param))


class TypeClass(object):
    __metaclass__ = TypeClassMeta
    instances = {}

    @classmethod
    def get_instance(cls, obj):
        try:
            return next(v for k, v in cls.instances.iteritems()
                        if isinstance(obj, k))
        except StopIteration:
            raise TypeError('No instance for {}({})'.format(
                cls.__name__, get_name(type(obj))))


class Functor(TypeClass):
    @abc.abstractmethod
    def fmap(self, f, x):
        raise NotImplementedError('fmap')


def fmap(f, fx):
    instance = Functor.get_instance(fx)
    result = instance.fmap(f, fx)
    assert isinstance(result, instance.type_param)
    return result


class Applicative(Functor):
    @abc.abstractmethod
    def ap(self, f, x):
        raise NotImplementedError('ap')


def ap(f, ax, *bxs):
    instance = Applicative.get_instance(f)
    # If additional arguments passed, curry f so it won't attempt
    # to evaluate on the first pass.
    if bxs:
        f = fmap(curry(len(bxs) + 1), f)
    result = instance.ap(f, ax)
    for bx in bxs:
        result = instance.ap(result, bx)
    assert isinstance(result, instance.type_param)
    return result


class Monad(Applicative):
    @abc.abstractmethod
    def unit(self, x):
        pass

    @abc.abstractmethod
    def bind(self, x, f):
        pass

    def then(self, x, y):
        return self.bind(x, lambda _: y)


def bind(mx, f):
    instance = Monad.get_instance(mx)
    result = instance.bind(mx, f)
    assert isinstance(result, instance.type_param)
    return result


def then(mx, my):
    instance = Monad.get_instance(mx)
    result = instance.then(mx, my)
    assert isinstance(result, instance.type_param)
    return result


def wrap_m(obj):
    instance = Monad.get_instance(obj)

    class MonadWrapper(type(obj)):
        def bind(self, f):
            return MonadWrapper(instance.bind(self, f))

        def then(self, other):
            return MonadWrapper(instance.then(self, other))
    return MonadWrapper(obj)


def identity(x):
    return x


def const(x):
    def constant(*args, **kwargs):
        return x
    return constant


def flip(f):
    def flipped(y, x, *args, **kwargs):
        return f(x, y, *args, **kwargs)
    return flipped


def join(mx):
    instance = Monad.get_instance(mx)
    result = instance.bind(mx, identity)
    assert isinstance(result, instance.type_param)
    return result


def lift_m(f):
    def lifted(mx):
        instance = Monad.get_instance(mx)
        return instance.bind(mx, lambda x: instance.unit(f(x)))
    return lifted


def curry(n):
    def wrapper(f):
        def curried(*args):
            if len(args) >= n:
                return f(*args)
            return partial(curried, *args)
        return curried
    return wrapper


def auto_curry(f):
    return curry(f.func_code.co_argcount)(f)


# Instances.


class ListFunctor(Functor(list)):
    def fmap(self, f, xs):
        if not xs:
            return []
        return map(f, xs)


class DictFunctor(Functor(dict)):
    def fmap(self, f, d):
        if not d:
            return {}
        return {k: f(v) for k, v in d.iteritems()}


class ListApplicative(Applicative(list)):
    def ap(self, fs, xs):
        return [f(x) for f in fs for x in xs]


class ListMonad(Monad(list)):
    def unit(self, x):
        return [x]

    def bind(self, x, f):
        return list(itertools.chain.from_iterable(map(f, x)))


class Maybe(object):
    __metaclass__ = abc.ABCMeta

    bind = bind
    then = then
    fmap = flip(fmap)
    ap = flip(ap)

    @staticmethod
    def from_nullable(x):
        if x is None:
            return Nothing
        return Just(x)

    @classmethod
    def from_nullables(cls, *xs):
        return map(cls.from_nullable, xs)

    def to_nullable(self):
        if self is Nothing:
            return None
        return self._x

    @abc.abstractmethod
    def __init__(self):
        pass


class Just(Maybe):
    def __init__(self, x):
        self._x = x

    def __repr__(self):
        return 'Just({!r})'.format(self._x)

    def __eq__(self, other):
        if not isinstance(other, Just):
            return False
        return self._x == other._x

    def __ne__(self, other):
        return not (self == other)

    def __iter__(self):
        yield self._x


class Nothing(Maybe):
    def __init__(self):
        pass

    def __repr__(self):
        return 'Nothing'

Nothing = Nothing()


class MaybeFunctor(Functor(Maybe)):
    def fmap(self, f, mx):
        if mx is Nothing:
            return Nothing
        [x] = mx
        return Just(f(x))


class MaybeApplicative(Applicative(Maybe)):
    def ap(self, mf, mx):
        if mf is Nothing or mx is Nothing:
            return Nothing
        [f] = mf
        [x] = mx
        return Just(f(x))


class MaybeMonad(Monad(Maybe)):
    def unit(self, x):
        return Just(x)

    def bind(self, mx, f):
        if mx is Nothing:
            return Nothing
        [x] = mx
        return f(x)


class State(object):
    __rshift__ = then = then

    def __init__(self, f):
        self._f = f

    def run(self, x):
        return self._f(x)

    def __iter__(self):
        yield self._f


class StateMonad(Monad(State)):
    def unit(self, x):
        return State(lambda s: (x, s))

    def bind(self, mx, f):
        def new_f(s):
            [h] = mx
            a, new_state = h(s)
            [g] = f(a)
            return g(new_state)
        return State(new_f)

    # HACK: These should actually be implemented in their appropriate
    # typeclasses.  Really, an error should be raised for this since
    # we did not define instances of Functor or Applicative.
    def fmap(self, f, x):
        raise NotImplementedError

    def ap(self, f, x):
        raise NotImplementedError

